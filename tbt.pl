#!/usr/bin/perl

use strict;
use warnings;
use CGI;
use Data::Dumper 'Dumper';

my $numVotes = 10;


my $q = CGI->new;
my $om = OptionManager->new();

my @options;
my $genOptions = 1;
for (my $i = 0; $i < $numVotes; ++$i) {

  if ($genOptions) {
    @options = showOptions();
  }
  $genOptions = 1;

  #chomp(my $vote = <STDIN>);

  my $vote;

  if ($options[0]->key() > $options[1]->key()) {
    $vote = 1;
  }
  else {
    $vote = 2;
  }

  if (!$vote) {
    last;
  }

  if ($vote != 1 and $vote != 2) {
    print "Invalid\n";
    $genOptions = 0;
    next;
  }

  my $for;
  my $against;
  if ($vote == 1) {
    $for = $options[0];
    $against = $options[1];
  }
  else {
    $for = $options[1];
    $against = $options[0];
  }

  $om->voteOption($for, $against);

}

my $order = $om->getOptions();
$order->sort();

my $n = 1;
for my $option (@{$order->getArray()}) {
  printf("%2d: %s\n", $n++, $option->name());
}

print $order->score(\@options);

$om->close();


sub showOptions {
  my @options = $om->getOptionsToCompare();
  my $n = 1;
  for my $option (@options) {
    #print $n++, ". ", $option->name(), "\n";
  }
  return @options;
}

sub voteForOption {
  my $option1 = shift;
  my $option2 = shift;


}

package OptionDatabaseManager;

use DBI;

sub new {
  my $self = shift;
  return bless {
    dbh => undef
  }, $self;
}

sub getOptions {
  my $self = shift;
  my $dbh = $self->dbh();

  my $query = $dbh->prepare('SELECT option_id, option_title FROM options');
  $query->execute();

  my $result = $query->fetchall_arrayref();

  my @ret;
  foreach (@$result) {
    my $id = $_->[0];
    my $title = $_->[1];
    push(@ret, Option->new($id, $title));
  }
  return \@ret;
}

sub getTwoOptions {
  my $self = shift;
  my $dbh = $self->dbh();

  my $query = $dbh->prepare('SELECT option_id, option_title FROM options ORDER BY RANDOM() LIMIT 2');
  $query->execute();

  my $result = $query->fetchall_arrayref();

  my @ret;
  foreach (@$result) {
    my $id = $_->[0];
    my $title = $_->[1];
    push(@ret, Option->new($id, $title));
  }
  return \@ret;
}

sub voteForOption {
  my $self = shift;

  my $posOpt = shift;
  my $negOpt = shift;


  my $dbh = $self->dbh();
  
  # step 1: normalize the order of posOpt vs negOpt
  my $option1 = $posOpt;
  my $option2 = $negOpt;

  if ($option1->cmpOpt($option2)) {
    my $tmp = $option1;
    $option1 = $option2;
    $option2 = $tmp;
  }

  # step 2: prepare: insert into database by increment/decrement score (increment if positive option is on left after normalize, negative otherwise)
  
  my $change = 1;
  if ($posOpt == $option2) {
    $change = -1;
  }

  # step 3: execute
  my $updateHandler = $dbh->prepare_cached("UPDATE option_edges SET score = score + ? WHERE option1 = ? AND option2 = ?");
  $updateHandler->execute($change, $option1->key(), $option2->key());
}

sub getOption {
  my $self = shift;
  my $key = shift;

  my $option = $self->{optionMap}{$key};
  if (!$option) {
    $option = Option->new($key);
    $self->{optionMap}{$key} = $option;
  }

  return $option;
}

sub getEdge {
  my $self = shift;
  
  my $key1 = shift;
  my $key2 = shift;
  
  if ($key1 > $key2) {
    my $tmp = $key1;
    $key1 = $key2;
    $key2 = $tmp;
  }

  my $dbh = $self->{dbh};

  

}

sub dbh {
  my $self = shift;
  if (!$self->{dbh}) {
    my $dbfile = "db";
    $self->{dbh} = DBI->connect("dbi:SQLite:dbname=$dbfile","","",
    {RaiseError => 1}
    );
  }
  return $self->{dbh};
}

sub close {
  my $self = shift;
  return if !$self->{dbh}; # no connect: quit out

  my $dbh = $self->{dbh};
  $dbh->disconnect();
  $self->{dbh} = undef;
}

###############

package OptionManager;
use DBI;
use List::Util; # this is a hack: replace it with something better

use Data::Dumper qw/Dumper/;

sub new {
  my $self = shift;
  my $edgeMap = {};
  my $optionMap = {};
  my $db = OptionDatabaseManager->new();
  my @options = 1 .. 10;

  $self = bless {
    edgeMap => $edgeMap,
    optionMap => $optionMap,
    db => $db
  }, $self;

  return $self;
}

sub close {
  my $self = shift;
  $self->{db}->close();
}

sub getOptions {
  my $self = shift;
  my $db = $self->{db};
  my $optList = $db->getOptions();
  return OptionList->new($om, $optList);
}

sub getOptionsToCompare {
  my $self = shift;
  my $db = $self->{db};

  my $optList = $db->getTwoOptions();

  return @$optList;
}

sub compareOptions {
  my $self = shift;
  my $option1 = shift;
  my $option2 = shift;
  my $edge = $self->getEdge($option1, $option2);
  my $score = $edge->score($option1);
}

sub voteOption {
  my $self = shift;

  my $db = $self->{db};

  my $votedForOpt = shift;
  my $votedAgainstOpt = shift;

  $db->voteForOption($votedForOpt, $votedAgainstOpt);

  my $edge = $self->getEdge($votedForOpt, $votedAgainstOpt);

  $edge->voteFor($votedForOpt);

}

sub getEdge {
  my $self = shift;
  my $option1 = shift;
  my $option2 = shift;

  if ($option1->cmpOpt($option2)) {
    my $tmp = $option1;
    $option1 = $option2;
    $option2 = $tmp;
  }

  my $key1 = $option1->key();
  my $key2 = $option2->key();

  my $edge = $self->{edgeMap}{$key1}{$key2};
  if (!$edge) {
    $edge = OptionEdge->new($option1, $option2);
    $self->{edgeMap}{$key1}{$key2} = $edge;
  }

  return $edge;
}

###############

package OptionEdge;

sub new {
  my $self = shift;
  my $option1 = shift;
  my $option2 = shift;

  return bless { 
    score => 0,
    posOpt => $option1
  }, $self;
}

sub score {
  my $self = shift;
  my $fromOpt = shift;

  if ($fromOpt == $self->{posOpt}) {
    return $self->{score};
  } else {
    return -1 * $self->{score};
  }
}

sub voteFor {
  my $self = shift;
  my $votedForOption = shift;
  
  if ($self->{posOpt} == $votedForOption) {
    ++$self->{score};
  } else {
    --$self->{score};
  }
}

###############

package Option;

sub new {
  my $self = shift;
  my $id = shift;
  my $title = shift;

  return bless { title => $title, id => $id }, $self;
}

sub name {
  my $self = shift;
  return $self->{title};
}

sub key {
  my $self = shift;
  return $self->{id};
}

sub cmpOpt {
  my $self = shift;
  my $otherOption = shift;

  return $self->key() > $otherOption->key();
}

sub serialize {
  return shift->name();
}

###############

package OptionList;

sub new {
  my $self = shift;
  my $om = shift;
  my $options = shift;
  
  return bless {
    list => $options,
    om => $om
  }, $self;
}

sub score {
  my $self = shift;

  my $options = $self->{list};
  my $om = $self->{om};

  my $score = 0;

  for (my $i = 0; $i < @$options; ++$i) {
    for (my $j = $i; $j < @$options; ++$j) {
      my $a = $options->[$i];
      my $b = $options->[$j];
      my $compareScore = $om->compareOptions($a, $b);
      $score += $compareScore
    }
  }

  return $score;
}

sub swap {
  my $self = shift;
  my $i = shift;
  my $j = shift;
  my $list = $self->{list};
  _swap($i, $j, $list);
}

sub getArray {
  my $self = shift;
  return $self->{list};
}

sub serialize {
  my $self = shift;
  my $ret = join ",", map { $_->serialize() } @{$self->{list}};

  $ret .= " (" . $self->score() . ")";

  return $ret;
}

sub _swap {
  my $i = shift;
  my $j = shift;
  my $array = shift;
  my $tmp = $array->[$i];
  $array->[$i] = $array->[$j];
  $array->[$j] = $tmp;
}

sub sort {
  my $self = shift;
  my $om = $self->{om};

  my $check = 1;
  while ($check) {
    my $score = $self->score();
    $check = 0;

    for (my $i = 1; $i < @{$self->getArray()}; ++$i) {
      $self->swap($i - 1, $i);
      my $newScore = $self->score();
      if ($newScore > $score) {
        $score = $newScore;
        $check = 1;
      }
      else {
        # swap back
        $self->swap($i - 1, $i);
      }
    }
  }

  return $self;
}
