#!/usr/bin/perl

use DBI;

open OPTIONS, "options" or die "Cannot option file: $!";

my $dbfile = "db";
my $dbh = DBI->connect("dbi:SQLite:dbname=$dbfile","","");

my @options;
my $k = 1;

foreach (<OPTIONS>) {
  chomp;
  my $insertHandler = $dbh->prepare_cached("INSERT INTO options(option_title) VALUES(?)");
  push @options, $k++;
  $insertHandler->execute($_);
}

close OPTIONS;

for (my $i = 0; $i < @options; ++$i) {
  for (my $j = $i + 1; $j < @options; ++$j) {
    my $insertHandler = $dbh->prepare_cached("INSERT INTO option_edges(option1,option2,score) VALUES(?,?,0)");
    $insertHandler->execute($options[$i],$options[$j]);
  }
}

