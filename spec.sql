DROP TABLE options;
DROP TABLE option_edges;
CREATE TABLE options (
  option_id integer primary key not null,
  option_title varchar(100) not null unique
);
CREATE TABLE option_edges (
  option1 integer REFERENCES options(option_id),
  option2 integer REFERENCES options(option_id),
  score integer default 0
);
